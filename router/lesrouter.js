const {
  callMeWithYourLanguage,
  Articles,
  resetData,
} = require("../controller/index");
const { chatReq } = require("../controller/chatgpt");
module.exports = (app) => {
  app.get("/fr/:nbr", async (req, res) => {
    TargetURL = "https://fr.hespress.com/";
    await callMeWithYourLanguage(req.params.nbr);
    res.send(Articles);
    await resetData();
  });
  app.get("/en/:nbr", async (req, res) => {
    TargetURL = "https://en.hespress.com/";
    await callMeWithYourLanguage(req.params.nbr);
    res.send(Articles);
    await resetData();
  });
  app.get("/ar/:nbr", async (req, res) => {
    TargetURL = "https://hespress.com/";
    await callMeWithYourLanguage(req.params.nbr);
    res.send(Articles);
    await resetData();
  });
  app.get("/", (req, res) => {
    res.send(Object.assign({ Home: "This is the Home Page" }, owner));
  });
  app.post("/chatgpt", async (req, res) => {
    await chatReq(req, res);
  });
};
