const express = require("express");

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

require("./router/lesrouter")(app);
app.use(({ res }) => {
  const messge = "erro 404 ";
  res.status(404).json({ messge });
});
app.listen(port, () => console.log("salu"));
